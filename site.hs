{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
import           Hakyll.Web.Sass (sassCompilerWith)
import           Text.Sass.Options

------------------------------------------------------------------------------

config :: Configuration
config = defaultConfiguration
    { previewHost = "0.0.0.0"
    , previewPort = 8000
    }

sassOptions = SassOptions
    { sassPrecision         = 3
    -- Set sassOutputStyle to SassStyleCompressed for production
    , sassOutputStyle       = SassStyleExpanded
    , sassSourceComments    = False
    , sassSourceMapEmbed    = False
    , sassSourceMapContents = False
    , sassOmitSourceMapUrl  = True
    , sassIsIndentedSyntax  = True
    , sassIndent            = "    "
    , sassLinefeed          = "\n"
    , sassInputPath         = Nothing
    , sassOutputPath        = Nothing
    , sassPluginPaths       = Nothing
    , sassIncludePaths      = Nothing
    , sassSourceMapFile     = Nothing
    , sassSourceMapRoot     = Nothing
    , sassFunctions         = Nothing
    , sassHeaders           = Nothing
    , sassImporters         = Nothing
    }

main :: IO ()
main = hakyllWith config $ do
    match "static/**" $ do
        route   idRoute
        compile copyFileCompiler

    match "styles/default.sass" $ do
        route $ gsubRoute "styles/" (const "static/css/") `composeRoutes`
            setExtension "css"
        compile $ sassCompilerWith sassOptions

    match (fromList ["about.md", "contact.md"]) $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls


    match "index.md" $ do
        route $ setExtension "html"
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Home"                `mappend`
                    defaultContext
            pandocCompiler
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateCompiler


------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

