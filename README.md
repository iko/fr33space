# fr33space

A simple [Hakyll] theme with [angeley.es] colours for fr33domlover.

[Preview]


## License

HTML/CSS: [CC0]

Fonts: SIL Open Font License by their respective authors


[Hakyll]: https://jaspervdj.be/hakyll/
[Preview]: preview-desktop.png
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
[angeley.es]: https://angeley.es
